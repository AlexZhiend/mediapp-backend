package com.mitocode.repo;

import com.mitocode.model.SignosVitales;

public interface ISignosRepo extends IGenericRepo<SignosVitales, Integer>{

}
