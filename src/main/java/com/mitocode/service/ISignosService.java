package com.mitocode.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mitocode.model.SignosVitales;

public interface ISignosService extends ICRUD<SignosVitales, Integer>{
	
	Page<SignosVitales> listarSignosP(Pageable page);

}
